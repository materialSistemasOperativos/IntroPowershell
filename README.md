# Introducción a Powershell

## ¿Qué es Powershell?
Lenguaje de scripting (Dinámico) caracterizado por ser ***Orientado a Objetos***


## ¿Cómo usar?

### Windows
* Presionamos WIN+R
* Ejecutamos:
 * `powershell` (Si solo queremos la terminal)
 * `powershell_ise` (Si queremos la terminal y un IDE)

### Linux
* Opción 1: Crear una VM e instalar Windows. Consultar si es necesario: 
 * Ejemplo Camino largo => [Oracle VirtualBox](https://gitlab.com/sisop/powershell/wikis/Prepara-Entorno-de-Trabajo-Con-VBOX)
 * Ejemplo Camino corto => [Vagrant + VirtualBox](https://gitlab.com/sisop/powershell/wikis/preparar-entorno-de-trabajo-con-vbox-vagrant)
* Opción 2: Instalar Powershell Core.
 * Ejemplo de instalación:

```sh
curl https://packages.microsoft.com/keys/microsoft.asc | sudo apt-key add -
curl https://packages.microsoft.com/config/ubuntu/16.04/prod.list | sudo tee /etc/apt/sources.list.d/microsoft.list
sudo apt-get update
sudo apt-get install -y powershell
#Finalizada la instalación ejecutar:
powershell 
```

## El infaltable Hola Mundo
Abrimos una terminal de PowerShell y tipeamos
```powershell
$pathDirTrabajo = "" #Completar
$script = "$pathDirTrabajo\Saludo.ps1" #Completar
#Creamos un directorio de trabajo
New-Item -Path $pathDirTrabajo  -ItemType "directory"
#Creamos nuestro primer script
New-Item -Path $script  -ItemType "file"
#Escribimos nuestro script
"Write-Output ""Hola Mundo"" " | Out-File  $script 
#Ahora Vamos a Crear un alias para invocar a nuestro Script
New-Item -Path $profile.CurrentUserAllHosts -ItemType file -Force
"New-Alias Hola $script"| Out-File $profile.CurrentUserAllHosts
#Finalmente ejecutamos mediante el alias en una nueva sesión
Hola
```

## Información
**Recuerde a partir de ahora su mejor amigo es el cmdlet `Get-Help`. Puede conocerlo simplemente escribiendo en la consola de powershell `PS > Get-Help`**

Puede consultar el contenido de powershell que se dictará en las clases mediante la wiki: 
 * [Documentando scripts](https://gitlab.com/sisop/IntroPowershell/wikis/Documentaci%C3%B3n)
 * [Powershell y los Objetos](https://gitlab.com/sisop/IntroPowershell/wikis/objetos)
 * [Operadores](https://gitlab.com/sisop/IntroPowershell/wikis/operadores)
 * [Ciclos](https://gitlab.com/sisop/IntroPowershell/wikis/loop)
 * [IF](https://gitlab.com/sisop/IntroPowershell/wikis/if)
 * [Switch](https://gitlab.com/sisop/IntroPowershell/wikis/switch)
 * [Variables especiales de powershell](https://gitlab.com/sisop/IntroPowershell/wikis/variables-especiales)
 * [Control de errores](https://gitlab.com/sisop/IntroPowershell/wikis/control-de-errores)
 * [Cadenas](https://gitlab.com/sisop/IntroPowershell/wikis/cadenas)
 * [Variables (Vectores, matrices, array asociativos)](https://gitlab.com/sisop/IntroPowershell/wikis/espacios-de-memorias)
 * [Redireccionamiento](https://gitlab.com/sisop/IntroPowershell/wikis/redirecci%C3%B3n)
 * [Funciones](https://gitlab.com/sisop/IntroPowershell/wikis/Funciones)
 * [Parametros](https://gitlab.com/sisop/IntroPowershell/wikis/par%C3%A1metros)
 * [CSV](https://gitlab.com/sisop/IntroPowershell/wikis/csv)
 * [Jobs](https://gitlab.com/sisop/IntroPowershell/wikis/jobs)
 * [Timer](https://gitlab.com/sisop/IntroPowershell/wikis/timer)

No olvide consultar el apuntes de la cátedra: [PowerShell.pdf](http://www.sisop.com.ar/files/catedra/apuntes/ps/PowerShell.pdf)


# Cómo entregar el Práctico

* Carátula impresa, a entregar por cada entrega. ***COMPLETAR CAMPOS SOLICITADOS Y RESPETAR LO INDICADO ENTRE PARÉNTESIS***
* Archivo comprimido con el siguiente contenido:
 * Una carpeta por ejercicio, su titulo será EjX
 * Es obligatorio que cada ejercicio incluya un lote de prueba propio. El mismo se compondrá  de la entrada y un script disparador para la ejecución  del mismo.
 * Puede utilizar el siguiente script PS para generar la estructura: 
 
 ```powershell
    $grupo = 1 #Complete el numero de grupo
    1..6 | % { mkdir "Powershell\G$grupo\EJ $_\Source"; mkdir "Powershell\G$grupo\EJ $_\Test"; }
 ```
*NOTA: asegurarse que los script a entregar funcionen en el laboratorio*

# Invitación a participar en este Repositorio

* Cualquier duda que quiera compartirse o contenido útil que considere, está autorizado a iniciarlo como una Issue. (En caso de dudas, esta permitido que participen los alumnos.)
* `No está permitido subir tp's corregidos.` 

*Nota: no se contestarán preguntas estilo ¿Cómo se hace tal punto del tp? :)*

[![Autor](https://img.shields.io/badge/Autor%20-Adrian%20Radice-blue.svg)](https://gitlab.com/adrianRadice)